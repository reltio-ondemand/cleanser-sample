```
#!txt

Copyright 2016 Reltio 100 Marine Parkway, Suite 275, Redwood Shores, CA USA 94065 (855) 360-DATA www.reltio.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```

[TOC]

# Overview #

This article describes high level steps to create a simple cleans function.

## Prequisites ##

* Understanding of Java, use of Java 8
* Familiarity with Maven

# How to create a new cleans function #

1. Create a new Java project based on the source files of the current repository "cleanser-sample"
2. Change the GroupId, ArtifactId, and Version inside the pom.xml file.
3. Add needed changes to java class files. 
4. Build the project (for example, execute mvn clean package) to get a JAR file.
5. Pass certification described in [Reltio Documentation Portal](https://docs.reltio.com/datacleanse/cleansefunccert.html)

# Additional information
For more information about deployment of the jar file and configuring the tenants, please visit
## [Reltio Help Desk](https://docs.reltio.com/datacleanse/setcleansefuncconfig.html)