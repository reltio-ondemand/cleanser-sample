package com.customername.cleanse.example.test;

import com.customername.cleanse.example.CleanseFunctionExample;
import com.reltio.sdk.cleanse.CleanseException;
import com.reltio.sdk.cleanse.CleanseInputs;
import com.reltio.sdk.cleanse.CleanseOutputs;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CleanserFunctionExample {

    protected Logger logger = LogManager.getLogger(CleanserFunctionExample.class);

    private static final CleanseFunctionExample function = new CleanseFunctionExample();

    @Test
    public void test1() throws CleanseException {
        CleanseInputs input = new CleanseInputs("entities/hjh6ut1");
        input.addCleanseAttribute("fn", "John");
        input.addCleanseAttribute("ln", "Taylor");

        List<CleanseOutputs> outputList =
                function.cleanseValues(Arrays.asList(input), Collections.emptyMap(), Collections.emptyMap());

        String fullName = (String) outputList.get(0).getCleansedAttributes().get("result").get(0);
        Assert.assertEquals("John Taylor", fullName);
    }

    @Test
    public void test2() throws CleanseException {
        CleanseInputs input = new CleanseInputs("entities/hjh6ut1");
        input.addCleanseAttribute("ln", "Taylor");

        List<CleanseOutputs> outputList =
                function.cleanseValues(Arrays.asList(input), Collections.emptyMap(), Collections.emptyMap());

        Assert.assertTrue("cleanser returning output when no output expected", outputList.isEmpty());
    }
}
