package com.customername.cleanse.example;

import com.reltio.sdk.cleanse.CleanseException;
import com.reltio.sdk.cleanse.CleanseInputs;
import com.reltio.sdk.cleanse.CleanseOutputs;
import com.reltio.sdk.cleanse.ICleanseFunction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CleanseFunctionExample implements ICleanseFunction {
    protected Logger logger = LogManager.getLogger(CleanseFunctionExample.class);
    @Override
    public List<CleanseOutputs> cleanseValues(List<CleanseInputs> list, Map<String, String> map, Map<String, String> map1) throws CleanseException {

        List<CleanseOutputs> result = new ArrayList<>(list.size());
        for (CleanseInputs input : list) {
            String fn = extractAttribute("fn", input);
            String ln = extractAttribute("ln", input);
            if (fn != null && ln != null) {
                fn = fn.trim();
                ln = ln.trim();
                if (!fn.isEmpty() && !ln.isEmpty()) {
                    CleanseOutputs output = input.createOutputs();
                    result.add(output);
                                output.addCleansedAttribute("result", fn + " " + ln);
                }
            }
        }
        logger.info("Inputs = " + list.stream().map(input->input.getAttributesToCleanse()).collect(Collectors.toList())+
                ", output = " + result.stream().map(input->input.getCleansedAttributes()).collect(Collectors.toList()));
        return result;
    }

    private String extractAttribute(String name, CleanseInputs input) {
        Object ob = input.getAttributesToCleanse().get(name);
        if (ob instanceof List) {
            List list = (List)ob;
            if (!list.isEmpty()) {
                ob = list.get(0);
            }
        }
        if (ob instanceof String) {
            return (String)ob;
        }
        return null;
    }
}
